# iOS Code Challenge

## Overview 

Please create a Swift-based iOS app that downloads a text file from our server, parses the contents, and displays the results.

## Download 

You may retrieve the file at the following URL:

https://bitbucket.org/dealerinspire/ios-code-challenge/raw/master/Apache.log

## Parse 

The file is a standard Apache web server access log. We’re interested in knowing the most common three-page sequences in the file. A three-page sequence is three consecutive requests from the same user. In case it helps simplify your processing, you can assume the following about the contents:

- Log entries are in ascending chronological order
- An IP address is unique per user


## Display 

Please present the results in descending order by sequence frequency, so the most common sequence appears first and the least common appears last. Each sequence should display the pages that make up the sequence and the number of times that the sequence appears. A table view seems like a reasonable way to present the results, but you are free to pick a different method if you think it is more appropriate.


## Example 

Based on the following sample data, sequence “Page 1, Page 2, Page 3” is the most common. It appears twice in the results:

User A: Page 1

User B: Page 1
User B: Page 2
User B: Page 3
User B: Page 2

User A: Page 2
User A: Page 3
User A: Page 4
User A: Page 1
User A: Page 2
